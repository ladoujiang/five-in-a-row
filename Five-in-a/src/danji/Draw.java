package danji;
import java.awt.*;
import java.awt.event.*;
import java.util.Stack;
import javax.swing.*;

public class Draw extends JPanel implements MouseListener ,ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Image boardImg;//抽象类 Image 是表示图形图像的所有类的超类。
    public Image profile_picture;
    Five2 fiv;
    final  int rows=16;
    int qq,pp;
    int  imgWidth;//图片的宽度
	 int  imgHeight;//图片的高度
	 int FWidth;//容器的宽度
	 int FHeight;//容器的高度
	 int x;//左空白宽
	 int y;
	 int x0; //（x0,y0）鼠标位置
	 int y0;
	 int span_x;//单元格的宽度
	 int span_y;//单元格的高度
	 int i;
	 int j;
	 int victory; //胜利
	 Victory_face2 vif;
   // TextField field;
    JButton button_start,button_defeat,button_regret ;
    ButtonGroup option;
	JRadioButton optionA;
	JRadioButton optionB;
    String name1 ;
	
	JFrame frame ;
	JPanel panel ;
	int person ;  //黑白方
	int colour1,colour2;//控制棋子颜色
	Robot ro;
	int[][]a = new int[15][15] ;//记录棋子
	Stack<Integer> stack;
      //构造方法
      public Draw() { 
           //实例化对象DrawBoard
    	  for (i = 0; i < 15; i++) {
              for (j = 0; j < 15; j++) {a[i][j]=0;}}  
    	  Shuju2.ii=Shuju2.jj=-1;
    	  Shuju2.number=0;
    	  colour1=1;colour2=-1;
    	  stack=new Stack<Integer>();
    	  frame = new JFrame();
    	  fiv=new Five2();
    	  ro = new Robot();
    	  vif = new Victory_face2();
          	 button_start = new JButton("开始");
          	 button_defeat = new JButton("认输");
          	 button_regret = new JButton("悔棋");
          	option = new ButtonGroup();
    		optionA = new JRadioButton("黑子");
    		optionB = new JRadioButton("白子");
    		option.add(optionA);
    		optionA.setSelected(true);
    		option.add(optionB);
    		add(optionA);
    		add(optionB);         	 
            add(button_start);
         	add(button_defeat);
         	add(button_regret);          	 
          	button_start.addActionListener(this);
          	button_defeat.addActionListener(this);
          	button_regret.addActionListener(this);          	           	 
            name1 = "辣豆浆001" ; //用户信息
            String picture_id = "head.jpg" ;
          	profile_picture = Toolkit.getDefaultToolkit().getImage(picture_id);          	 
          	boardImg = Toolkit.getDefaultToolkit().getImage("gobang.jpg");
            if (boardImg == null || profile_picture == null) {
                   System.out.println("Image do not exit");
               }        
          person = 1;
          frame.setTitle("软工小组-五子棋新春帅哥版");  
          frame.setExtendedState(Frame.MAXIMIZED_BOTH); //frame全屏
      //    frame.setSize(1000,1000);
          frame.setResizable (true);
          frame.setVisible(true);
          frame.setLocationRelativeTo(null);
          //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
          frame.add(this); //将drawBoard组件添加到容器                    
          frame.addMouseListener(this);         
      }
      public void paintComponent(Graphics g){
     	 super.paintComponent(g);    	 
     	imgWidth=boardImg.getWidth(this);//图片的宽度
     	 imgHeight=boardImg.getHeight(this);//图片的高度
     	 FWidth=getWidth();//容器的宽度
     	  FHeight=getHeight();//容器的高度
     	  x=(FWidth-imgWidth)/3;//左右空白宽
     	 y=(FHeight-imgHeight)/2;
     	 g.drawImage(boardImg,x,y,null);//画出图片
     	 g.drawImage(profile_picture, FWidth-x-100,y,200,200, null);    	 
     	 g.setFont(new Font("宋体",Font.BOLD,40));//昵称字体大小
     	 g.drawString(name1, FWidth-x-90, y+250); 
     	 optionA.setFont(new Font("宋体", Font.BOLD, 40));
     	 optionA.setBounds(x/3, 50, 120, 90);
     	 optionB.setFont(new Font("宋体", Font.BOLD, 40));
     	 optionB.setBounds(x/3, 150, 120, 90);
     	 button_start.setFont(new Font("宋体", Font.BOLD, 30));
     	 button_start.setBounds(x/3, 300, 120, 90);
     	 button_defeat.setFont(new Font("宋体", Font.BOLD, 30));
     	 button_defeat.setBounds(x/3, 500, 120, 90);
     	 button_regret.setFont(new Font("宋体", Font.BOLD, 30));
     	 button_regret.setBounds(x/3, 700, 120, 90);    		 
     	 span_x=imgWidth/rows;//单元格的宽度
     	 span_y=imgHeight/rows;//单元格的高度    	 
     	//画横线
     	 for(i=1;i<rows;i++){
     		 g.drawLine(x+span_x, y+i*span_y,FWidth-2*x-span_x,y+i*span_y);
     	 }
     	 //画竖线  
     	 for(i=1;i<rows;i++){  
     		 g.drawLine(x+i*span_x, y+span_y, x+i*span_x,FHeight-y-span_y);  
     	 }  
     	 //棋盘的4个黑点
     	 Color c1 = g.getColor();
     	 g.setColor(Color.black);
     	 g.fillOval(x-5+span_x*4, y-5+span_y*4 ,10,10);
     	 g.fillOval(x-5+span_x*8, y-5+span_y*8 ,10,10);
     	 g.fillOval(x-5+span_x*4, y-5+span_y*12,10,10);
     	 g.fillOval(x-5+span_x*12,y-5+span_y*4 ,10,10);
     	 g.fillOval(x-5+span_x*12,y-5+span_y*12,10,10);
     	 g.setColor(c1);
     	 
          for (i = 0; i < 15; i++) {
              for (j = 0; j < 15; j++) {
                  if (a[i][j] == 1)
                  {
                	  Color c3 = g.getColor();
                 	 g.setColor(Color.black);
                 	 g.fillOval(x-25+span_x*(i+1),y-25+span_y*(j+1), 50, 50);
                 	g.setColor(c3);
                  }
                  if(a[i][j]== -1)
                  {
                	  Color c2 = g.getColor();
                 	 g.setColor(Color.white);
                 	 g.fillOval(x-25+span_x*(i+1),y-25+span_y*(j+1), 50, 50);
                 	g.setColor(c2);
                 	}
                  
                  }
              } 
          i=Shuju2.ii;j=Shuju2.jj;
          if ((i+j)!=-2) g.drawRect(x-25+span_x*(i+1),y-25+span_y*(j+1),50, 50);
     	 }
      
      public void mousePressed(MouseEvent e) {
          // TODO Auto-generated method stub
          if(e.getButton() ==MouseEvent.BUTTON1 && person==1){  
        	  // 判断获取的按钮是否为鼠标的右击     
        	  x0=(int) e.getX();
        	  y0=(int) e.getY(); 
        	  for (i=0;i<rows;i++)        		  
        		  for (j=0;j<rows;j++)
        	  {
        		  if ((x0-x-span_x*(i+1))*(x0-x-span_x*(i+1))+(y0-y-span_y*(j+2))*(y0-y-span_y*(j+2))<400)
        		  {  
        			  if(a[i][j]==0)
        		  {
        			  a[i][j]=colour1; 
        			  Shuju2.ii=i;Shuju2.jj=j;
        			  stack.push(j);
        			  stack.push(i);
        			  person = -1;
        			  Shuju2.number++;
        			  victory=fiv.vic(a, i, j,1);
        			  if(victory!=0) person=0;
        			  break;
        		  }   
        			  }
        	  }
        	  if (person==-1) {
        		  ro.RB(a);
        	  i=Shuju2.ii;
        	  j=Shuju2.jj;
        	  a[i][j]=colour2;
        	  stack.push(j);
        	  stack.push(i);
        	  System.out.println(i+"___"+j+"白棋");
        	  person=1;
        	  Shuju2.number++;
        	  victory=fiv.vic(a, i, j,1);
			  if(victory!=0) person=0;
        	  }
        	repaint();
        	
           }
        }
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub		
	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub	
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub	
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		if (e.getSource()== button_start)
		{
			Shuju2.number=0;
			Shuju2.ii=Shuju2.jj=-1;
  		  for (i = 0; i < 15; i++) {
  			  for (j = 0; j < 15; j++) {a[i][j]=0;
  			  }
  		}  if(optionA.isSelected() == true)
		{
  			colour1=1;colour2=-1;
		}
  		  if(optionB.isSelected() == true)
		{
			a[7][7]=1;
			stack.push(7);
			stack.push(7);
			Shuju2.ii=Shuju2.jj=7;
			colour1=-1;colour2=1;
			Shuju2.number++;
		}  
  		person=1;
  		repaint();
  	}
		if (e.getSource()== button_defeat)
		{
			if (person==1) {
			vif.face(colour2);
			victory=colour2;}
			if (person==-1) {
				vif.face(colour1);
				victory=colour1;}
			person=0;
		}
		if(e.getSource()==button_regret)
		{
			a[stack.pop()][stack.pop()]=0;
			a[stack.pop()][stack.pop()]=0;
			Shuju2.ii=stack.pop();Shuju2.jj=stack.pop();
			stack.push(Shuju2.jj);
			stack.push(Shuju2.ii);
			Shuju2.number=Shuju2.number-2;
			person=1;
			repaint();
		}
	}
}   