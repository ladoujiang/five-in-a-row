package danji;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.Stack;

import javax.swing.ButtonGroup;

//import java.util.EventListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
//import gobang.DrawChessBoard;  
public class Draw1 extends JPanel implements MouseListener ,ActionListener,Runnable//
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Image boardImg;//抽象类 Image 是表示图形图像的所有类的超类。
    public Image profile_picture;
    Five1 fiv;
    final  int rows=16;
    int qq,pp;
    int  imgWidth;//图片的宽度
	 int  imgHeight;//图片的高度
	 int FWidth;//容器的宽度
	 int FHeight;//容器的高度
	 int x;//左空白宽
	 int y;
	 int x0; //（x0,y0）鼠标位置
	 int y0;
	 int span_x;//单元格的宽度
	 int span_y;//单元格的高度
	 int i;
	 int j;
	 int victory; //胜利
	 Victory_face1 vif;
    JButton button_start,button_defeat,button_regret ;
    ButtonGroup option;
	JRadioButton optionA;
	JRadioButton optionB;
    String name1 ;
	
	JFrame frame ;
	JPanel panel ;
	int person ;  //黑白方  
	int[][]a = new int[15][15] ;//记录棋子
	int number;
	Send1 send;
	int colour1,colour2;//棋子颜色
	Stack<Integer> stack;
      //构造方法
      public Draw1() { 
           //实例化对象DrawBoard
    	  for (i = 0; i < 15; i++) {
              for (j = 0; j < 15; j++) {a[i][j]=0;}}  
    	  person = 0;
    	  number=-1;
    	  Shuju.number=-1;
    	  Shuju.ii=Shuju.ii1=-1;
    	  Shuju.jj=Shuju.jj1=-1;
    	  colour1=1;colour2=-1;
    	  send = new Send1();
    	  stack=new Stack<Integer>();
    	  frame = new JFrame();
    	  fiv=new Five1();
    	  vif = new Victory_face1();
          	 button_start = new JButton("开始");
          	 button_defeat = new JButton("认输");
          	 button_regret = new JButton("悔棋");
          	 add(button_start);
          	 add(button_defeat);
          	 add(button_regret);
          	option = new ButtonGroup();
    		optionA = new JRadioButton("黑子");
    		optionB = new JRadioButton("白子");
    		option.add(optionA);
    		optionA.setSelected(true);
    		option.add(optionB);
    		add(optionA);
    		add(optionB);
          	button_start.addActionListener(this);
          	button_defeat.addActionListener(this);
          	button_regret.addActionListener(this);
          	 name1 = "辣豆浆001" ; //用户信息
          	 String picture_id = "C:\\Users\\17925\\Desktop\\head.jpg     " ;
          	 profile_picture = Toolkit.getDefaultToolkit().getImage(picture_id);         
               boardImg = Toolkit.getDefaultToolkit().getImage("C:\\Users\\17925\\Desktop\\gobang.jpg");
               if (boardImg == null || profile_picture == null) {
                   System.out.println("Image do not exit");
               }        
          
          frame.setTitle("软工小组-五子棋新春帅哥版");  
         frame.setExtendedState(Frame.MAXIMIZED_BOTH); //frame全屏
       //   frame.setSize(1000,1000);
          frame.setResizable (true);
          frame.setVisible(true);
          frame.setLocationRelativeTo(null);
       //   frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         frame.add(this); //将drawBoard组件添加到容器      
          frame.addMouseListener(this);
      }
      public void paintComponent(Graphics g){
     	 super.paintComponent(g);    	 
     	imgWidth=boardImg.getWidth(this);//图片的宽度
     	 imgHeight=boardImg.getHeight(this);//图片的高度
     	 FWidth=getWidth();//容器的宽度
     	  FHeight=getHeight();//容器的高度
     	  x=(FWidth-imgWidth)/3;//左右空白宽
     	 y=(FHeight-imgHeight)/2;
     	 g.drawImage(boardImg,x,y,null);//画出图片
     	 g.drawImage(profile_picture, FWidth-x-100,y,200,200, null);    	 
     	 g.setFont(new Font("宋体",Font.BOLD,40));//昵称字体大小
     	 g.drawString(name1, FWidth-x-90, y+250);    	
         optionA.setFont(new Font("宋体", Font.BOLD, 40));
    	 optionA.setBounds(x/3, 50, 120, 90);
    	 optionB.setFont(new Font("宋体", Font.BOLD, 40));
    	 optionB.setBounds(x/3, 150, 120, 90);
    	 button_start.setFont(new Font("宋体", Font.BOLD, 30));
     	 button_start.setBounds(x/3, 300, 120, 90);
     	 button_defeat.setFont(new Font("宋体", Font.BOLD, 30));
     	 button_defeat.setBounds(x/3, 500, 120, 90);
     	 button_regret.setFont(new Font("宋体", Font.BOLD, 30));
     	 button_regret.setBounds(x/3, 700, 120, 90);    		   	 
     	 span_x=imgWidth/rows;//单元格的宽度
     	 span_y=imgHeight/rows;//单元格的高度    	 
     	//画横线
     	 for(i=1;i<rows;i++){
     		 g.drawLine(x+span_x, y+i*span_y,FWidth-2*x-span_x,y+i*span_y);
     	 }
     	 //画竖线  
     	 for(i=1;i<rows;i++){  
     		 g.drawLine(x+i*span_x, y+span_y, x+i*span_x,FHeight-y-span_y);  
     	 }  
     	 //棋盘的4个黑点
     	 Color c1 = g.getColor();
     	 g.setColor(Color.black);
     	 g.fillOval(x-5+span_x*4, y-5+span_y*4 ,10,10);
     	 g.fillOval(x-5+span_x*8, y-5+span_y*8 ,10,10);
     	 g.fillOval(x-5+span_x*4, y-5+span_y*12,10,10);
     	 g.fillOval(x-5+span_x*12,y-5+span_y*4 ,10,10);
     	 g.fillOval(x-5+span_x*12,y-5+span_y*12,10,10);
     	 g.setColor(c1);
          for (i = 0; i < 15; i++) {
              for (j = 0; j < 15; j++) {
                  if (a[i][j] == colour1)
                  {
                	  Color c3 = g.getColor();
                 	 g.setColor(Color.black);
                 	 g.fillOval(x-25+span_x*(i+1),y-25+span_y*(j+1), 50, 50);
                 	g.setColor(c3);
                  }
                  if(a[i][j]== colour2)
                  {
                	  Color c2 = g.getColor();
                 	 g.setColor(Color.white);
                 	 g.fillOval(x-25+span_x*(i+1),y-25+span_y*(j+1), 50, 50);
                 	g.setColor(c2);
                 	}
                  }
              }
          
          i=Shuju.ii;j=Shuju.jj;
     //   if (i>=0)  a[i][j]=-1;
       number=Shuju.number;
       if (number==1) {person=1;a[i][j]=-1;Shuju.number=2;stack.push(j);
		  stack.push(i);}
    	  if (number==-11) {optionB.setSelected(true);}
    	  if (number==-12) {optionA.setSelected(true);}
    	  if (number==-13) {

    		  if(optionA.isSelected() == true)
    			{
    				vif.face(-1);
  			    victory=-1;
    			} 
    			if(optionB.isSelected() == true)
    			{
    				vif.face(1);
  				victory=1;
    			}  
  			person=0;
  			Shuju.number=-1;
    	  }
    	  if (number==-14) {
    		  if(optionA.isSelected() == true)
    			{
    				vif.face(1);
  			    victory=1;
    			} 
    			if(optionB.isSelected() == true)
    			{
    				vif.face(-1);
  				victory=-1;
    			}  
  			person=0;
  			Shuju.number=-1;
    	  }
    	  if (number==-15) {
    		  qq=stack.pop();
    		  pp=stack.pop();
    		  a[qq][pp]=0;
    		  qq=stack.pop();
    		  pp=stack.pop();
    		  a[qq][pp]=0;
    		  qq=stack.pop();
    		  pp=stack.pop();
    		  Shuju.jj1=pp;
    		  Shuju.ii1=qq;    		
    		  stack.push(pp);
    		  stack.push(qq);
    		 Shuju.number=0;
    	  }
        if (number==0) {i=Shuju.ii1;j=Shuju.jj1;}
          if ((i+j)!=-2) g.drawRect(x-25+span_x*(i+1),y-25+span_y*(j+1),50, 50);
     	 
     	 }
      public void rp() {
    	  
    	  repaint();
    	  
      }
      public void mousePressed(MouseEvent e) {
          // TODO Auto-generated method stub
          if(e.getButton() ==e.BUTTON1 && person==1){  
        	  // 判断获取的按钮是否为鼠标的右击     
        	
        	  x0=(int) e.getX();
        	  y0=(int) e.getY(); 
        	  for (i=0;i<rows;i++)        		  
        		  for (j=0;j<rows;j++)
        	  {
        		  if ((x0-x-span_x*(i+1))*(x0-x-span_x*(i+1))+(y0-y-span_y*(j+2))*(y0-y-span_y*(j+2))<400)
        		  {  
        			  if(a[i][j]==0)
        		  {
        			
        			  a[i][j]=1; 
        			  stack.push(j);
        			  stack.push(i);
        			  Shuju.ii1=i;Shuju.jj1=j;
        			  Shuju.number=0;
        			  send.send(i*16+j);        			  
        			  victory=fiv.vic(a, i, j, colour1);
        			  if(victory==1) {
        				  person=0;
        				  send.send(-13);
        				  }
        			  }
        			  
        			  break;
        			  }
        		  
        	  }
        	         	
        	  person=-1;
        	  repaint();
        	 }
        }
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub		
	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub	
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub	
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(optionA.isSelected() == true)
			{
				colour1=1;colour2=-1;
				send.send(-11);
				person=1;
			} 
			if(optionB.isSelected() == true)
			{
				colour1=-1;colour2=1;
				send.send(-12);
				person=-1;
			}  
		if (e.getSource()== button_start)
		{
  		  for (i = 0; i < 15; i++) {
  			  for (j = 0; j < 15; j++) {a[i][j]=0;
  			  }}
  			  Shuju.ii=Shuju.ii1=-1;
  			  Shuju.jj=Shuju.jj1=-1;
  			if(optionA.isSelected() == true)
  			{
  				colour1=1;colour2=-1;
  				send.send(-11);
  				person=1;
  			} 
  			if(optionB.isSelected() == true)
  			{
  				colour1=-1;colour2=1;
  				send.send(-12);
  				person=-1;
  			}  
  			 stack=new Stack<Integer>();
  		repaint();
  	}
		if (e.getSource()== button_defeat)
		{
			send.send(-14);
			if(optionA.isSelected() == true)
  			{
  				vif.face(-1);
			    victory=-1;
  			} 
  			if(optionB.isSelected() == true)
  			{
  				vif.face(1);
				victory=1;
  			}  
			person=0;
		}
		if (e.getSource()== button_regret && person==1) {
			qq=stack.pop();
			pp=stack.pop();
			a[qq][pp]=0;
			qq=stack.pop();
			pp=stack.pop();
			a[qq][pp]=0;
			qq=stack.pop();
			pp=stack.pop();
			Shuju.jj=pp;
			Shuju.ii=qq;
			stack.push(pp);
			stack.push(qq);
			repaint();
			send.send(-15);
		}
	}
	@Override
	public void run() {
		 synchronized (this){ rp();}
		// TODO Auto-generated method stub
		
	}
	
	
}   