package gobang;
//import java.awt.Color.*;
import java.awt.*;
import javax.swing.*;
public class DrawBoard extends JPanel {
     public Image boardImg;//抽象类 Image 是表示图形图像的所有类的超类。
     public Image profile_picture;
     final private int ROWS=16;
     TextField field;
     JButton button_start,button_defeat,button_regret ;
     String name1 ;
     public DrawBoard() {
    	 field = new TextField(100);
    //	 add(field,BorderLayout.SOUTH);
    	 button_start = new JButton("开始");
    	 button_defeat = new JButton("认输");
    	 button_regret = new JButton("悔棋");
    	 add(button_start);
    	 add(button_defeat);
    	 add(button_regret);
    	 name1 = "辣豆浆001" ; //用户信息
    //	 addMouseListener(this);
    	 profile_picture = Toolkit.getDefaultToolkit().getImage("C:\\Users\\17925\\Desktop\\head.jpg");         
         boardImg = Toolkit.getDefaultToolkit().getImage("C:\\Users\\17925\\Desktop\\gobang.jpg");
         if (boardImg == null || profile_picture == null) {
             System.out.println("Image do not exit");
         }
     }
     //重写
     protected void paintComponent(Graphics g){
    	 super.paintComponent(g);
    	 
    	 
    	 int  imgWidth=boardImg.getWidth(this);//图片的宽度
    	 int  imgHeight=boardImg.getHeight(this);//图片的高度
    	 int FWidth=getWidth();//容器的宽度
    	 int FHeight=getHeight();//容器的高度
    	 int x=(FWidth-imgWidth)/4;//左右空白宽
    	 int y=(FHeight-imgHeight)/2;
    	 g.drawImage(boardImg,x,y,null);//画出图片
    	 g.drawImage(profile_picture, FWidth-2*x,y,200,200, null);
    	 
    	 g.setFont(new Font("宋体",Font.BOLD,40));//昵称字体大小
    	 g.drawString(name1, FWidth-2*x+5, y+250);
    	 
    	 button_start.setBounds(x/2, 250, 90, 60);
    	 button_defeat.setBounds(x/2, 450, 90, 60);
    	 button_regret.setBounds(x/2, 650, 90, 60);
    	
    //	 int margin=x;
    	 
    	 
    	 
    	 int span_x=imgWidth/ROWS;//单元格的宽度
    	 int span_y=imgHeight/ROWS;//单元格的高度
    	 
    	//画横线
    	 for(int i=1;i<ROWS;i++){
    		 g.drawLine(x+span_x, y+i*span_y,FWidth-3*x-span_x,y+i*span_y);
    	 }
    	 //画竖线  
    	 for(int i=1;i<ROWS;i++){  
    		 g.drawLine(x+i*span_x, y+span_y, x+i*span_x,FHeight-y-span_y);  
    	 }  
    	 //棋盘的4个黑点
    	 Color c1 = g.getColor();
    	 g.setColor(Color.black);
    	 g.fillOval(x-5+span_x*4, y-5+span_y*4 ,10,10);
    	 g.fillOval(x-5+span_x*8, y-5+span_y*8 ,10,10);
    	 g.fillOval(x-5+span_x*4, y-5+span_y*12,10,10);
    	 g.fillOval(x-5+span_x*12,y-5+span_y*4 ,10,10);
    	 g.fillOval(x-5+span_x*12,y-5+span_y*12,10,10);
    	 g.setColor(c1);
    	 
    	 int a,b;
    	 a=4;
    	 b=4;
    	 Color c2 = g.getColor();
    	 g.setColor(Color.white);
    	 g.fillOval(x-25+span_x*a,y-25+span_y*b, 50, 50);
    	 g.fillOval(x-25+span_x*(a+1),y-25+span_y*b, 50, 50);
    	 g.setColor(c2);
    	 
    	 
    	// g.drawOval(x-25+span_x*a,y-25+span_y*b,50,50);
    	 
    	 
    	 
    	
    	 
    	 }
 }