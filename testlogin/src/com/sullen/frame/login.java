package com.sullen.frame;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.*;

import com.sullen.util.ColorfulCAPTCHALabel;

public class login extends JFrame{
private static final long serialVersionUID = -4655235896173916415L;
private JPanel contentPane;
private JTextField usernameTextField;
private JPasswordField passwordField;
private JTextField validateTextField;
private String randomText;
public static void main(String args[]){
try {
UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
} catch (Throwable e) {
e.printStackTrace();
}
EventQueue.invokeLater(new Runnable(){
public void run(){
try{
login frame=new login();
frame.setVisible(true);
}catch(Exception e){
e.printStackTrace();
}
}
});

}
public login(){
setTitle("系统登录");
setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
contentPane=new JPanel();
setContentPane(contentPane);
contentPane.setLayout(new BoxLayout(contentPane,BoxLayout.PAGE_AXIS));

JPanel usernamePanel=new JPanel();
contentPane.add(usernamePanel);

JLabel usernameLable=new JLabel("\u7528\u6237\u540D\uFF1A");
usernameLable.setFont(new Font("微软雅黑", Font.PLAIN, 15));
usernamePanel.add(usernameLable);

usernameTextField=new JTextField();
usernameTextField.setFont(new Font("微软雅黑", Font.PLAIN, 15));
usernamePanel.add(usernameTextField);
usernameTextField.setColumns(10);

JPanel passwordPanel = new JPanel();
contentPane.add(passwordPanel);
JLabel passwordLabel = new JLabel("\u5BC6 \u7801\uFF1A");
passwordLabel.setFont(new Font("微软雅黑", Font.PLAIN, 15));
passwordPanel.add(passwordLabel);
passwordField = new JPasswordField();
passwordField.setColumns(10);
passwordField.setFont(new Font("微软雅黑", Font.PLAIN, 15));
passwordPanel.add(passwordField);
JPanel validatePanel = new JPanel();
contentPane.add(validatePanel);
JLabel validateLabel = new JLabel("\u9A8C\u8BC1\u7801\uFF1A");
validateLabel.setFont(new Font("微软雅黑", Font.PLAIN, 15));
validatePanel.add(validateLabel);
validateTextField = new JTextField();
validateTextField.setFont(new Font("微软雅黑", Font.PLAIN, 15));
validatePanel.add(validateTextField);
validateTextField.setColumns(5);
randomText = RandomStringUtils.randomAlphanumeric(4);
ColorfulCAPTCHALabel label = new ColorfulCAPTCHALabel(randomText);//随机验证码
label.setFont(new Font("微软雅黑", Font.PLAIN, 15));
validatePanel.add(label);

JPanel buttonPanel=new JPanel();
contentPane.add(buttonPanel);

JButton submitButton=new JButton("登录");
submitButton.addActionListener(new ActionListener() {
@Override
public void actionPerformed(ActionEvent e) {
do_submitButton_actionPerformed(e);
}

private void do_submitButton_actionPerformed(ActionEvent e) {
	// TODO Auto-generated method stub
	
}
});
submitButton.setFont(new Font("微软雅黑", Font.PLAIN, 15));
buttonPanel.add(submitButton);

JButton cancelButton=new JButton("退出");
cancelButton.addActionListener(new ActionListener(){
public void actionPerformed(ActionEvent e){
do_cancelButton_actionPerformed(e);
}

private void do_cancelButton_actionPerformed(ActionEvent e) {
	// TODO Auto-generated method stub
	
}
});
cancelButton.setFont(new Font("微软雅黑",Font.PLAIN,15));
buttonPanel.add(cancelButton);

pack();// 自动调整窗体大小
setLocation(com.sullen.util.SwingUtil.centreContainer(getSize()));// 让窗体居中显示

}
}
